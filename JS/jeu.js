﻿$(document).ready(function () {

/**
 * ****************************************Pour AJAX selon le navigateur************************************************
 */
    // Appel ajax pour l'envoi de données à la page verifJoueur.php
    function getRequest() {
        var xhr = null;

        if (window.XMLHttpRequest) // Autre navigateur que Internet Explorer
        {
            xhr = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) // Internet Explorer
        {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else  // XMLHttpRequest non supporté par le navigateur
        {
            alert("Ce navigateur ne supporte pas les objets XMLHTTPRequest.");
            return false;
        }
        return xhr;
    }


/**
 ****************************************Pour le timer image de chargement**********************************************
 */
    // A l'ouverture de la fenêtre on démarre une fonction qui va declencher la fonction du timer
    $(window).load(function () {
        compteARebour();

    });
    var dejaLancee = true;
    //au clic du choix pour le joueur on annonce que la fonction est cliquée et on met notre cerf de chargement
    $('#choixJoueur').change(function () {
        if(dejaLancee){
            $(this).data('clicked', true);
            document.getElementById('info3').innerHTML = '<img src=../image/cerf.gif alt="image de chargement">';
            masquerChoixJoueur();
            dejaLancee = false;
        }
    });

    //fonction du chrono, qui se stop quand on arrive a 0 et si le joueur clique sur sur un choix
    var sec = 5;

    function compteARebour() {
        var finChrono = setTimeout(compteARebour, 1000);
        document.getElementById("info3").innerHTML = "<h1 class=\"rouge taille\">" + sec + "</h1>";
        sec--;
        if (sec < 0) {
            sec = 0;
            document.getElementById("info3").innerHTML = "<h1 class=\"rouge taille\"><span class=\"bleu\">P</span>ERDU <span class=\"bleu\">!</span></h1>";
            $('#bouton').css("display", "block");
            clearTimeout(finChrono);
        }
        if (jQuery('#choixJoueur').data('clicked')) {
            clearTimeout(finChrono);
            setTimeout(envoi, 2000);
            document.getElementById('info3').innerHTML = '<img src=../image/cerf.gif alt="image de chargement">';
        }
    }


/**
 ***********************************Retour des données PHP avec AJAX et traitement**************************************
 */
    //une fois le choix fait on l'envoi sur la page php en ajax. et on affiche si le resultat de la partie
    function envoi() {
        document.getElementById('info3').innerHTML = '';
        // fonction ajax envoi de données
        var xhr;
        var message;
        var reponseAjax;
        var choixOrdi;
        var choixJoueur = $('input[name=Joueur]:checked', '#choixJoueur').val();
        var resultatJoueur;
        var resultatOrdinateur;
        var totalParties;
        var data = '?choix=' + choixJoueur;
        xhr = getRequest();

        if (xhr !== false) {
            xhr.open("GET", "../php/calcul.php" + data, true);
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {

                    reponseAjax = xhr.responseText;
                    reponseAjax = reponseAjax.split(',');
                   // console.log(reponseAjax.join());
                    choixOrdi = reponseAjax[0]; // on récupère le choix de l'ordinateur
                    message = reponseAjax[1]; // on récupère le résultat correspondant

                    resultatJoueur = reponseAjax[2]; //on recupère les points joueur
                    resultatOrdinateur = reponseAjax[3]; //on recupère les points ordinateur
                    totalParties = reponseAjax[4]; //on recupère le nombre de tentatives
                    masquerChoixOrdi(choixOrdi); // doit être appelée du moment qu'on a la réponse du serveur*/

                    //  console.log (message);
                }
                else if (xhr.readyState === 2 || xhr.readyState === 3) {
                    // pour éviter qu'il affiche erreur alors qu'il est en chargement
                    message = '<img src=../image/cerf.gif alt="image de chargement">';
                }
                else {
                    message = "<p>Erreur dans l'appel du fichier de calcul</p>";
                }
                document.getElementById('info3').innerHTML = message;
                document.getElementById('resultatJoueur').innerHTML = resultatJoueur;
                document.getElementById('resultatOrdinateur').innerHTML = resultatOrdinateur;


            };
            xhr.send(null);
            $('#bouton').css("display", "block");
        }
    }


/**
 *************************************Pour mettre en évidence les choix des parties*************************************
 */
    function masquerChoixJoueur() {
        //dans la var id on récupère l'id de l'input checked
        var id = $('input[name=Joueur]:checked', '#choixJoueur').attr('id');
        //on récpère le label correspondant à l'id
        var label = $("label[for='" + id + "']");
        /*lorsque qu'il y a un un input checked, tous les labels se grisent mais celui de  l'id
         correspondant au choix réaparait */
        if ($('input[name=Joueur]').is(':checked')) {
            $('label', '#choixJoueur').fadeTo(500, 0.2);
            label.fadeTo(250, 1);
        }
    }

    function masquerChoixOrdi(choixOrdi) {
        var id;

        if (choixOrdi == 1) {
            id = "pierreOrdi";
        } else if (choixOrdi == 2) {
            id = "feuilleOrdi";
        } else if (choixOrdi == 3) {
            id = "ciseauxOrdi";
        }

        var label = $("label[for='" + id + "']");
        $('label', '#choixOrdi').fadeTo(500, 0.2);
        label.fadeTo(250, 1);
    }

/**
 *********************************fonction pour le bouton de redirection à la page recap********************************
 */
    $('#non').click(function () {
        document.location.href = "recapitulatif.php";
    });


    //fonction pour le bouton qui permet de recommencer une partie
    $('#oui').click(function () {
        location.reload();
        $('#bouton').css("display", "none");
        //$( "#choixJoueur").unbind( "click" );
        //$("#choixJoueur").data('clicked', false);
    });
});


    
    
    

    
    


