﻿$(document).ready(function () {
    /**
     * ******************************************Pour AJAX selon le navigateur******************************************
     */

    //appel ajax pour l'envoie de données a la page verifJoueur.php
    function getRequest() {
        var xhr = null;

        if (window.XMLHttpRequest) // Autre navigateur que Internet Explorer
        {
            xhr = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) // Internet Explorer
        {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else  // XMLHttpRequest non supporté par le navigateur
        {
            alert("Ce navigateur ne supporte pas les objets XMLHTTPRequest.");
            return false;
        }
        return xhr;
    }

    /**
     * ******************************************Pour la connexion du joueur********************************************
     */

    // Fonction au clic qui verifie et fait un appel ajax
    /*
     * Fonction anonyme, si on met un paramètre il faut directement le définir...je pense qu'on en a pas besoin ici
     * (j'enlève donc le paramètre "function(verificationConnection)"
     */
    $('#submit1').click(function () {
        if (testNom()) {
            // fonction ajax envoi de données
            var xhr;
            var affiche;
            var nomJoueur = $('#name1').val();
            var data = '?id=' + nomJoueur;
            xhr = getRequest();

            if (xhr !== false) {
                xhr.open("GET", "php/verifJoueur.php" + data, true);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        affiche = xhr.responseText;
                    }

                    else {
                        affiche = "Erreur dans l'appel du fichier d'inscription";
                    }
                    document.getElementById('info1').innerHTML = affiche;
                };
                xhr.send(null);
            }
        }
    });


    // Fonction de vérification du nom du joueur
    function testNom() {
        var nomJoueur = $('#name1').val();
        var regex = new RegExp(/^\w+$/);
        if (nomJoueur === "") {
            $('#info1').html("<p>N'oublies pas de choisir un pseudo !</p>");
            return false;
        }
        if (regex.test(nomJoueur) === false) {
            $('#info1').html("<p>Outch ! Ton pseudo est trop compliqué !</p>");
            return false;
        }
        if (nomJoueur.length < 7 || nomJoueur.length > 7) {
            $('#info1').html("<p>Ton pseudo doit comporter 7 caractères</p>");
            return false;
        }
        return true;
    }


    /**
     ********************************************Pour l'inscription du joueur*******************************************
     */
    // Fonction au clic qui verifie et fait un appel ajax
    $('#submit2').click(function () {
        if (validiteNom()) {

            // fonction ajax envoi de données
            var xhr;
            var affiche;
            var nomJoueur = $('#name2').val();
            var data = '?nom=' + nomJoueur;
            xhr = getRequest();

            if (xhr !== false) {
                xhr.open("GET", "php/inscriptionJoueur.php" + data, true);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        affiche = xhr.responseText;
                    }

                    else {
                        affiche = "Erreur dans l'appel du fichier d'inscription";
                    }
                    document.getElementById('info2').innerHTML = affiche;
                };
                xhr.send(null);
            }
        }
    });

    // Fonction de vérification du nom du joueur
    function validiteNom() {
        var nomJoueur = $('#name2').val();
        var regex = new RegExp(/^\w+$/);
        if (nomJoueur === "") {
            $('#info2').html("<p>N'oublies pas de choisir un pseudo !</p>");
            return false;
        }
        if (regex.test(nomJoueur) === false) {
            $('#info2').html("<p>Outch ! Ton pseudo est trop compliqué !</p>");
            return false;
        }
        if (nomJoueur.length < 3 || nomJoueur.length > 30) {
            $('#info2').html("<p>Ton pseudo doit comporter 7 caractères</p>");
            return false;
        }
        return true;
    }
});