﻿<?php

require_once 'bdd.php';

/**
 ********************************Pour vérifier que l'identifiant respecte le bon format**********************************
 */
    if (isset($_GET['id']) && !empty ($_GET['id'])) {

        // identifiant a une longueur valide
        $longueur_chaine = strlen($_GET['id']);
        if ($longueur_chaine < 7 || $longueur_chaine > 7) {
            die("La longueur de l'identifiant est non valide.<br>");
        }

        // L'identifiant est bien formé de caractères alphanumériques
        if (!preg_match("/^[a-zA-Z]{3}[0-9]{4}/", $_GET['id'])) {
            die("L'identifiant est non valide.<br>");
        }

/**
 ****************************************Pour vérifier qu'il existe bien en BDD*****************************************
 */

    $id = $_GET["id"];

    $requete = "SELECT * FROM joueur WHERE id = :id";
    $query = getPDO()->prepare($requete);
    $query->bindParam(':id', $id);
    $query->execute();
    $resultat = $query->fetch();

    if ($resultat === false) {
        die ("Impossible de trouver l'utilisateur");
    } else {
        session_start();
        $resultat['partieGagnee'] = intval($resultat['partieGagnee']);
        $resultat['totalPartie'] = intval($resultat['totalPartie']);
        $_SESSION['joueur'] = $resultat;
        echo '<p><span class="align">Prêt à jouer ?</span><a class="align" href="php/jeu.php" title="figth"><img src="image/fight.png"/></a></p>';
    }

} else {
    die("Identifiant non valide");
}
?>