﻿<?php
require_once 'bdd.php';
session_start ();

/**
 ********* recuperation des resultats du joueur*************
 */

$id=$_SESSION['joueur']['id'];

    $requete = "SELECT * FROM joueur WHERE id = :id";
    $query = getPDO()->prepare($requete);
    $query->bindParam(':id', $id);
    $query->execute();
    $resultat = $query->fetch();
     
    
    if($resultat === false){
       die ("Impossible de trouver vos resulats");
    } else {
        echo 'Felicitation'.'<br>'.$resultat['nomJoueur'] . ",";
        echo 'Nombre de parties jou�es'.'<br>'.$resultat['totalPartie']. ",";
        echo 'Nombre de parties gagn�es'.'<br>'.$resultat['partieGagnee'] . ",";
    }
/**
 *********** affichage dans tableau des scores**************
 */
    
    $requeteTableau = "SELECT * FROM joueur ORDER BY partieGagnee ";
    $query = getPDO()->prepare($requeteTableau);
    $query->execute();
    $resultatTableau = $query->fetch();  

    if($resultatTableau === false){
       die ("Impossible d'afficher le tableau des score");
    } else {
        while ( $resultatTableau = $query->fetch() ) {
            echo $resultatTableau['nomJoueur'].$resultatTableau['totalPartie'].'<br>' .",";
        }
    }
    
?>

