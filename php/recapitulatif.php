﻿<?php
require_once 'bdd.php';
session_start ();

/**
 *************** recuperation des resultats du joueur******************************
 */

$id=$_SESSION['joueur']['id'];

    $requete = "SELECT * FROM joueur WHERE id = :id";
    $query = getPDO()->prepare($requete);
    $query->bindParam(':id', $id);
    $query->execute();
    $resultat = $query->fetch();
     
    
    if($resultat === false){
       die ("Impossible de trouver vos resulats");
    }
/**
 *********** affichage dans tableau des scores**************
 */
    
     
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <link rel="stylesheet" href="../style.css"/>
        <title>Jeux chifoumi</title>
    </head>
    
    <body>
        <header id="chrono">
            <a href="../index.html"><h1 class="rouge">Chifoumi</h1></a>
        </header>
        <section>
            <article id="joueurAffichage">
                <h2>
                    Félicitation<br>
                    <?php echo $resultat['nomJoueur'] ?>
                </h2>
                <h3 class="rouge">
                    Nombre de parties jouées<br>
                     <?php echo $resultat['totalPartie'] ?>
                </h3>
                <h3 class="bleu">
                    Nombre de parties gagnées <br>
                    <?php echo$resultat['partieGagnee']?>
                </h3>
                    
            </article>
            <article id="classementAffichage">
                <h2>Classement des meilleurs joueurs</h2>
                <div class="droite">
                    <p class="bleu">Nom</p>
                </div>
                <div class="gauche">
                    <p class="rouge"> Parties Gagnées</p>
                </div>
                <p id="classement espace">
                    <?php
                        $requeteTableau = "SELECT * FROM joueur ORDER BY partieGagnee DESC LIMIT 11";
                        $query = getPDO()->prepare($requeteTableau);
                        $query->execute();
                        $resultatTableau = $query->fetch();  

                        if($resultatTableau === false){
                           die ("Impossible d'afficher le tableau des score");
                        } else {
                            while ( $resultatTableau = $query->fetch() ) {
                                echo '<div class="droite"><p class="rows">'.$resultatTableau['nomJoueur'].'</p></div>'
                                        .'<div class="gauche"><p class="rows">'.$resultatTableau['partieGagnee'].'</p></div>';
                            }
                        } 
                    ?>
                </p> 
            </article>
            
        </section>
		<footer>
			<p>Copyright - Landraudie Marine 2015</p>
		</footer>
    </body>
</html>
