﻿<?php
require_once 'bdd.php';

if (isset($_GET['nom']) && !empty ($_GET['nom'])) {

    $longueur_chaine = strlen($_GET['nom']);
    if ($longueur_chaine < 3 || $longueur_chaine >20) {
        $erreur = true;
        echo("Le nom du client doit être compris entre 3 et 20 caractères.<br />");
    } else {
        // valide
        // faire l'ajout

        // generation id

        $nom = $_GET['nom'];
        $id = substr($nom, 0, 3) . rand(10, 99) . rand(10, 99);
        $partieGagnee = 0;
        $totalPartie = 0;

        $requete = "INSERT INTO joueur ( id, nomJoueur,   partieGagnee,    totalPartie)
                    VALUES             (:id, :nom,       :partieGagnee,   :totalPartie)";
        $query = getPDO()->prepare($requete);
        $query->bindParam(':id', $id);
        $query->bindParam(':nom', $nom);
        $query->bindParam(':partieGagnee', $partieGagnee);
        $query->bindParam(':totalPartie', $totalPartie);
        //      } catch (PDOException $pdoException){ // Obligatoire, un if/else serait sans effet

        //     echo $pdoException->getMessage();

        if ($query->execute()) {
            echo "<p>Bienvenue sur le jeu du chifoumi, votre identifiant de connexion est : " . $id;
            echo "<br>Veuillez vous connecter juste à gauche avec votre identifiant !</p>";
        } else {
            echo "Une erreur est survenue lors de votre inscription";
        }
    }

} else {
    //invalide
    echo("Le champ nom est vide");
}


?>