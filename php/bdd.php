﻿<?php
/**
 **************************************Mise en place de PDO pour la connection avec la BDD*******************************
 */

 function getPDO(){

        $moteur = "mysql";
        $hote = "localhost";
        $login = "root";
        $mdp = "";
        $base = "chifoumi";

        try {
            $bdd = new PDO($moteur . ':host=' . $hote . ';dbname=' . $base, $login, $mdp);
            // $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // TODO: LIRE CA http://php.net/manual/fr/pdo.error-handling.php
            $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $bdd;
    }



