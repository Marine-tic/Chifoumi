﻿<?php
require_once 'bdd.php';
session_start();


/**
 **************************************calcul pour le jeu de l'ordinateur***********************************************
 */
//stokage du choix aleatoire de l'ordinateur
    $ordinateur = rand(1,3);
    echo $ordinateur . ",";

//teste si l'envoie d'ajax n'est pas vide + stokage dans une variable si c'est good
    if (isset($_GET['choix']) && !empty ($_GET['choix'])) {
        if (!preg_match("/^[1-3]/", $_GET['choix'])) {
            die("La valeur est trafiquée<br>");
        }
        $joueur = $_GET["choix"];   
    }
    else {
        die("Ton choix est mal enregistré");
    }

/**
 *********************************comparaison du choix pour obtenir le resultat*****************************************
 */
    // on analyse les deux variables --> gagnant ou perdant
    $messageGagne = "<h1 class=\"rouge taille\"><span class=\"bleu\">G</span>AGNÉ <span class=\"bleu\">!</span></h1>";
    $messageEgalite = "<h1 class=\"rouge taille\"><span class=\"bleu\">E</span>GALITÉ <span class=\"bleu\">!</span></h1>";
    $messagePerdu = "<h1 class=\"rouge taille\"><span class=\"bleu\">P</span>ERDU <span class=\"bleu\">!</span></h1>";

   if($joueur - $ordinateur == 0){
        echo $messageEgalite . ",";
       $resultatJ = 0;
       $resultatO = 0;
    }
    if ($joueur - $ordinateur == -1 ){
        echo $messagePerdu. ",";
        $resultatJ = 0;
        $resultatO = 1;
    }

    if ($joueur - $ordinateur == -2 ){
        echo $messageGagne. ",";
        $resultatJ = 1;
        $resultatO = 0;
    }

    if ($joueur - $ordinateur == 1 ){
        echo $messageGagne. ",";
        $resultatJ = 1;
        $resultatO = 0;
    }

    if ($joueur - $ordinateur == 2 ){
        echo $messagePerdu. ",";
        $resultatJ = 0;
        $resultatO = 1;
    }

/**
 * *********************************Pour initialiser les variable de session********************************************
 */
    if(!isset($_SESSION['joueur']['pointJoueur'])) {
        $_SESSION['joueur']['pointJoueur'] = 0;
    }
    if(!isset($_SESSION['joueur']['pointOrdinateur'])) {
        $_SESSION['joueur']['pointOrdinateur'] = 0;
    }

    if(isset ($_SESSION['joueur'])){
        $_SESSION['joueur']['pointJoueur'] += $resultatJ;
        $_SESSION['joueur']['pointOrdinateur'] += $resultatO;
        $_SESSION['joueur']['partieGagnee'] += $resultatJ;

        // Mémorise le nombre de parties jouées pour la bdd
        $_SESSION['joueur']['totalPartie']++;
    }
    echo  $_SESSION['joueur']['pointJoueur'] . ",";
    echo  $_SESSION['joueur']['pointOrdinateur'] . ",";
    echo $_SESSION['joueur']['totalPartie'];



/**
*************************************** Pour insérer les données dans la BDD *******************************************
*/

    $partieGagnee = $_SESSION['joueur']['partieGagnee'];
    $totalPartie = $_SESSION['joueur']['totalPartie'];
    $id = $_SESSION['joueur']['id'];
    
    $requete = "UPDATE joueur SET partieGagnee = :partieGagnee, totalPartie = :totalPartie WHERE id= :id";
    $query = getPDO()->prepare($requete);
    $query->bindParam(':partieGagnee', $partieGagnee);
    $query->bindParam(':totalPartie', $totalPartie);
    $query->bindParam(':id', $id);
    $query->execute();
    