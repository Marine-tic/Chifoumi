﻿<?php
session_start();
if(!isset ($_SESSION['joueur'])){
    die("Veuillez vous connecter pour jouer");
} else {
    $joueur = $_SESSION['joueur'];
    $nom = $joueur['nomJoueur'];
    $partieGagnee = $joueur['partieGagnee'];
    $PointJoueur = 0;
    $PointOrdinateur = 0;
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="../JS/jeu.js"></script>
        <link rel="stylesheet" href="../style.css"/>
        <title>Jeux chifoumi</title>
    </head>
    <body>
        <header id="chrono">
            <a href="../index.html"><h1 class="rouge">Chifoumi</h1></a>
        </header>
        
        <section>
            <article id="joueur">
                <h2><?php echo "$nom" ?></h2>
                <div class="image">
                    <img src="../image/joueur.gif"  alt="image du joueur">
                </div>
                <div class="choix">
                    <form method="get" id="choixJoueur">
                        <input type="radio" class="radio" name="Joueur" id="pierreJoueur" value="1"/>
                        <label for="pierreJoueur"><img src="../image/pierre.gif" alt="pierre"></label>

                        <input type="radio" class="radio" name="Joueur" id="feuilleJoueur" value="2"/>
                        <label for="feuilleJoueur"><img src="../image/feuille.gif" alt="feuille"></label>

                        <input type="radio" class="radio" name="Joueur" id="ciseauxJoueur" value="3"/>
                        <label for="ciseauxJoueur"><img src="../image/ciseaux.gif" alt="ciseaux"></label>
                    </form>
                </div>
            </article>
            <article id="resultat">
                <div id="info3">
                    <h1 class="rouge taille">5</h1>
                </div>
                <div id="bouton">
                    <h2 class="bleu">Pas chichi de rejouer hein...</h2>
                    <input type="button" value="Oh que si" id="oui">
                    <input type="button" value="Je fuis..." id="non">
                <div>
            </article>
            
            <article id="ordinateur">
                <h2>ordinateur</h2>
                <div class="image">
                    <img  src="../image/ordinateur.gif"  alt="image de l'ordinateur">
                </div>
                <div class="choix">
                    <form method="get" id="choixOrdi">
                        <input type="radio" class="radio" name="Ordi" id="pierreOrdi" value="1"/>
                        <label for="pierreOrdi"><img src="../image/pierre.gif" alt="pierre"></label>

                        <input type="radio" class="radio" name="Ordi" id="feuilleOrdi" value="2"/>
                        <label for="feuilleOrdi"><img src="../image/feuille.gif" alt="feuille"></label>

                        <input type="radio" class="radio" name="Ordi" id="ciseauxOrdi" value="3"/>
                        <label for="ciseauxOrdi"><img src="../image/ciseaux.gif" alt="ciseaux"></label>
                    </form>
                </div>
            </article>
            <article id="recap">
                <div id="PointJoueur">
                    <h2><?php echo "$nom" ?></h2>
                    <h1 class="rouge taille" id="resultatJoueur"></h1>
                </div>
                <div id="PointOrdinateur">
                    <h2>Ordinateur</h2>
                    <h3 class="rouge taille" id="resultatOrdinateur"></h3>
                </div>

            </article>
        </section>
        <footer>
            <p>
                Copyright - Landraudie Marine 2015
            </p>
        </footer>
    </body>
</html>